/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MaxKorwinCejrowski
 */

import java.lang.Math;
import java.util.*;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class kalkulatorek extends javax.swing.JFrame {

    public static ScriptEngine eng;
    /**
     * Creates new form kalkulatorek
     */
    public kalkulatorek() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        current = new javax.swing.JTextField();
        equantion = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        byButt = new javax.swing.JButton();
        powerButt = new javax.swing.JButton();
        sqrtButt = new javax.swing.JButton();
        procentButt = new javax.swing.JButton();
        ceButt = new javax.swing.JButton();
        sevenButt = new javax.swing.JButton();
        fourButt = new javax.swing.JButton();
        oneButt = new javax.swing.JButton();
        negButt = new javax.swing.JButton();
        zeroButt = new javax.swing.JButton();
        commaButt = new javax.swing.JButton();
        equalsButt = new javax.swing.JButton();
        cButt = new javax.swing.JButton();
        eightButt = new javax.swing.JButton();
        fiveButt = new javax.swing.JButton();
        twoButt = new javax.swing.JButton();
        threeButt = new javax.swing.JButton();
        sixButt = new javax.swing.JButton();
        nineButt = new javax.swing.JButton();
        backButt = new javax.swing.JButton();
        divButt = new javax.swing.JButton();
        mulButt = new javax.swing.JButton();
        addButt = new javax.swing.JButton();
        subButt = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(159, 159, 159));

        current.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        current.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        current.setText("0");

        equantion.setHorizontalAlignment(javax.swing.JTextField.RIGHT);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(current)
            .addComponent(equantion, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(equantion, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(current, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.setBackground(new java.awt.Color(159, 159, 159));

        byButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        byButt.setText("1/x");
        byButt.setActionCommand("byX");
        byButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                byButtActionPerformed(evt);
            }
        });

        powerButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        powerButt.setText(" x²");
        powerButt.setActionCommand("squared");
        powerButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                powerButtActionPerformed(evt);
            }
        });

        sqrtButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        sqrtButt.setText("√");
        sqrtButt.setActionCommand("sqrt");
        sqrtButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sqrtButtActionPerformed(evt);
            }
        });

        procentButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        procentButt.setText("%");
        procentButt.setActionCommand("procent");
        procentButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                procentButtActionPerformed(evt);
            }
        });

        ceButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        ceButt.setText("CE");
        ceButt.setActionCommand("clean");
        ceButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ceButtActionPerformed(evt);
            }
        });

        sevenButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        sevenButt.setText("7");
        sevenButt.setActionCommand("seven");
        sevenButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sevenButtActionPerformed(evt);
            }
        });

        fourButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        fourButt.setText("4");
        fourButt.setActionCommand("four");
        fourButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fourButtActionPerformed(evt);
            }
        });

        oneButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        oneButt.setText("1");
        oneButt.setActionCommand("one");
        oneButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                oneButtActionPerformed(evt);
            }
        });

        negButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        negButt.setText("¬");
        negButt.setToolTipText("");
        negButt.setActionCommand("negate");
        negButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                negButtActionPerformed(evt);
            }
        });

        zeroButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        zeroButt.setText("0");
        zeroButt.setActionCommand("zero");
        zeroButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                zeroButtActionPerformed(evt);
            }
        });

        commaButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        commaButt.setText(",");
        commaButt.setToolTipText("");
        commaButt.setActionCommand("comma");
        commaButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                commaButtActionPerformed(evt);
            }
        });

        equalsButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        equalsButt.setText("=");
        equalsButt.setToolTipText("");
        equalsButt.setActionCommand("equals");
        equalsButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                equalsButtActionPerformed(evt);
            }
        });

        cButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        cButt.setText("C");
        cButt.setActionCommand("cleanAll");
        cButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cButtActionPerformed(evt);
            }
        });

        eightButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        eightButt.setText("8");
        eightButt.setActionCommand("eight");
        eightButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eightButtActionPerformed(evt);
            }
        });

        fiveButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        fiveButt.setText("5");
        fiveButt.setActionCommand("five");
        fiveButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fiveButtActionPerformed(evt);
            }
        });

        twoButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        twoButt.setText("2");
        twoButt.setActionCommand("two");
        twoButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                twoButtActionPerformed(evt);
            }
        });

        threeButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        threeButt.setText("3");
        threeButt.setActionCommand("three");
        threeButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                threeButtActionPerformed(evt);
            }
        });

        sixButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        sixButt.setText("6");
        sixButt.setActionCommand("six");
        sixButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sixButtActionPerformed(evt);
            }
        });

        nineButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        nineButt.setText("9");
        nineButt.setActionCommand("nine");
        nineButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nineButtActionPerformed(evt);
            }
        });

        backButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        backButt.setText("<-");
        backButt.setActionCommand("cleanLast");
        backButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backButtActionPerformed(evt);
            }
        });

        divButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        divButt.setText("÷");
        divButt.setActionCommand("divide");
        divButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                divButtActionPerformed(evt);
            }
        });

        mulButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        mulButt.setText("*");
        mulButt.setActionCommand("multiply");
        mulButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mulButtActionPerformed(evt);
            }
        });

        addButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        addButt.setText("+");
        addButt.setActionCommand("add");
        addButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtActionPerformed(evt);
            }
        });

        subButt.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        subButt.setText("-");
        subButt.setActionCommand("sub");
        subButt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                subButtActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(procentButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(sqrtButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(powerButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(byButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(negButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(zeroButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(commaButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(equalsButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(ceButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(backButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(divButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(oneButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(twoButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(threeButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(addButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(fourButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(fiveButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(sixButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(subButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(sevenButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(eightButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(nineButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(mulButt, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(byButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(powerButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sqrtButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(procentButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ceButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(backButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(divButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sevenButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(eightButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(nineButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(mulButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(fourButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fiveButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sixButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(subButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(oneButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(twoButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(threeButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(negButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(zeroButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(commaButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(equalsButt, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void zeroButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_zeroButtActionPerformed
        if(current.getText().equals("0")){}
            //current.setText("0");
        else
            current.setText(current.getText()+"0");
    }//GEN-LAST:event_zeroButtActionPerformed

    private void oneButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_oneButtActionPerformed
        if(current.getText().equals("0"))
            current.setText("1");
        else
            current.setText(current.getText()+"1");
    }//GEN-LAST:event_oneButtActionPerformed

    private void twoButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_twoButtActionPerformed
        if(current.getText().equals("0"))
            current.setText("2");
        else
            current.setText(current.getText()+"2");
    }//GEN-LAST:event_twoButtActionPerformed

    private void threeButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_threeButtActionPerformed
        if(current.getText().equals("0"))
            current.setText("3");
        else
            current.setText(current.getText()+"3");
    }//GEN-LAST:event_threeButtActionPerformed

    private void fourButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fourButtActionPerformed
        if(current.getText().equals("0"))
            current.setText("4");
        else
            current.setText(current.getText()+"4");
    }//GEN-LAST:event_fourButtActionPerformed

    private void fiveButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fiveButtActionPerformed
        if(current.getText().equals("0"))
            current.setText("5");
        else
            current.setText(current.getText()+"5");
    }//GEN-LAST:event_fiveButtActionPerformed

    private void sixButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sixButtActionPerformed
        if(current.getText().equals("0"))
            current.setText("6");
        else
            current.setText(current.getText()+"6");
    }//GEN-LAST:event_sixButtActionPerformed

    private void sevenButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sevenButtActionPerformed
        if(current.getText().equals("0"))
            current.setText("7");
        else
            current.setText(current.getText()+"7");
    }//GEN-LAST:event_sevenButtActionPerformed

    private void eightButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eightButtActionPerformed
        if(current.getText().equals("0"))
            current.setText("8");
        else    
            current.setText(current.getText()+"8");
    }//GEN-LAST:event_eightButtActionPerformed

    private void nineButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nineButtActionPerformed
        if(current.getText().equals("0"))
            current.setText("9");
        else
            current.setText(current.getText()+"9");
    }//GEN-LAST:event_nineButtActionPerformed

    private void ceButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ceButtActionPerformed
        
        current.setText("0");
    }//GEN-LAST:event_ceButtActionPerformed

    private void cButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cButtActionPerformed
        current.setText("0");
        equantion.setText("");
    }//GEN-LAST:event_cButtActionPerformed

    private void negButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_negButtActionPerformed
        if(!current.getText().equals("0"))
        {
        if(current.getText().contains("-"))
            current.setText(current.getText().replace("-", ""));
        else
            current.setText("-"+current.getText());
        }
    }//GEN-LAST:event_negButtActionPerformed

    private void commaButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_commaButtActionPerformed
        if(!current.getText().contains(","))
        current.setText(current.getText()+",");            
    }//GEN-LAST:event_commaButtActionPerformed

    private void backButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backButtActionPerformed
        if(current.getText().length()>1)
        {
            current.setText(current.getText().replaceFirst(".$",""));
        }
        else
        {
            current.setText("0");
        }
    }//GEN-LAST:event_backButtActionPerformed

    private void sqrtButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sqrtButtActionPerformed
        if(!current.getText().equals("0") && !current.getText().contains("-"))
        {
            current.setText(String.format("%.7f", Math.sqrt(Double.parseDouble(current.getText().replace(",", ".")))));
        }
        else
        {
            current.setText("0");
        }
    }//GEN-LAST:event_sqrtButtActionPerformed

    private void powerButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_powerButtActionPerformed
       if(!current.getText().equals("0"))
        {
            current.setText(String.format("%.0f", Math.pow(Double.parseDouble(current.getText().replace(",", ".")),2)));
        }
        else
        {
            current.setText("0");
        }
    }//GEN-LAST:event_powerButtActionPerformed

    private void byButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_byButtActionPerformed
        if(!current.getText().equals("0"))
        {
            current.setText(String.format("%.5f", 1/Double.parseDouble(current.getText().replace(",", "."))));
        }
        else
        {
            current.setText("0");
        }
    }//GEN-LAST:event_byButtActionPerformed

    private void addOperator(String operator)
    {
        String isNegative = current.getText();
        if(isNegative.charAt(0)=='-')
            equantion.setText(equantion.getText() + "(" + isNegative + ")" + operator);
        else
            equantion.setText(equantion.getText() + isNegative + operator);
        current.setText("0");
    }
    
    private void divButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_divButtActionPerformed
        addOperator("/");
    }//GEN-LAST:event_divButtActionPerformed

    private void mulButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mulButtActionPerformed
        addOperator("*");
    }//GEN-LAST:event_mulButtActionPerformed

    private void subButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_subButtActionPerformed
        addOperator("-");
    }//GEN-LAST:event_subButtActionPerformed

    private void addButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtActionPerformed
        addOperator("+");
    }//GEN-LAST:event_addButtActionPerformed

    private void equalsButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_equalsButtActionPerformed
        try{
            Object wynik = eng.eval((equantion.getText()+current.getText()).replace(",", "."));
            
            if(wynik instanceof Integer)
                current.setText(wynik.toString());
            else
                 current.setText(String.format("%.9f",  wynik));  
            equantion.setText("");
        }   
        catch(ScriptException e)
        {
            
        }
    }//GEN-LAST:event_equalsButtActionPerformed

    private void procentButtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_procentButtActionPerformed
        double procent = Double.parseDouble(current.getText().replace(",", "."));
        if(procent<1 && procent>0)
            current.setText(Double.toString(procent*100).replace(".", ","));            
    }//GEN-LAST:event_procentButtActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(kalkulatorek.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(kalkulatorek.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(kalkulatorek.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(kalkulatorek.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        eng = new ScriptEngineManager().getEngineByName("JavaScript");
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new kalkulatorek().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButt;
    private javax.swing.JButton backButt;
    private javax.swing.JButton byButt;
    private javax.swing.JButton cButt;
    private javax.swing.JButton ceButt;
    private javax.swing.JButton commaButt;
    private javax.swing.JTextField current;
    private javax.swing.JButton divButt;
    private javax.swing.JButton eightButt;
    private javax.swing.JButton equalsButt;
    private javax.swing.JTextField equantion;
    private javax.swing.JButton fiveButt;
    private javax.swing.JButton fourButt;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JButton mulButt;
    private javax.swing.JButton negButt;
    private javax.swing.JButton nineButt;
    private javax.swing.JButton oneButt;
    private javax.swing.JButton powerButt;
    private javax.swing.JButton procentButt;
    private javax.swing.JButton sevenButt;
    private javax.swing.JButton sixButt;
    private javax.swing.JButton sqrtButt;
    private javax.swing.JButton subButt;
    private javax.swing.JButton threeButt;
    private javax.swing.JButton twoButt;
    private javax.swing.JButton zeroButt;
    // End of variables declaration//GEN-END:variables
}
